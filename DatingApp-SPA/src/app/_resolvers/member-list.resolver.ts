import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { User } from '../_models/user';
import { UserService } from '../_services/user.service';
import { AlertifyService } from '../_services/alertify.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { UserParams } from '../_models/user-params';

@Injectable()
export class MemberListResolver implements Resolve<User[]> {
  pageNumber = 1;
  pageSize = 5;

  constructor(
    private userService: UserService,
    private router: Router,
    private alertify: AlertifyService
  ) {}

  resolve(route: ActivatedRouteSnapshot): Observable<User[]> {
    let userParams: UserParams = null;

    if (localStorage.getItem('userParams')) {
      userParams = JSON.parse(localStorage.getItem('userParams'));
    }
    return this.userService
      .getUsers(this.pageNumber, this.pageSize, userParams)
      .pipe(
        catchError((error) => {
          this.alertify.error('Problem retrieving data ' + error);
          this.router.navigate(['/home']);
          return of(null);
        })
      );
  }
}
